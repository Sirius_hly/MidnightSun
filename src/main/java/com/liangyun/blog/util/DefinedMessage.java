package com.liangyun.blog.util;

/**
 * @author :hly
 * @date :2018/4/7
 */
public class DefinedMessage {
    public String LOGIN_USERID_ERROR = "用户名不存在!";
    public String LOGIN_PASSWORD_ERROR = "密码错误!";
    public String LOGIN_USERID_DISABLED = "账号已被禁用!";
    public String LOGIN_NO_LOGED = "未登录!";
    public String LOGIN_SUCCESS = "登录成功!";
    public String LOGOUT_SUCCESS = "注销成功!";

    public String LOG_TYPE_LOGIN = "登陆";
    public String LOG_TYPE_LOGOUT = "注销";
    public String LOG_TYPE_ADD = "新增";
    public String LOG_TYPE_UPDATE = "更新";
    public String LOG_TYPE_DELETE = "删除";
    public String LOG_TYPE_COMPLETE = "完成";
    public String LOG_TYPE_IMPORT = "导入";
    public String LOG_TYPE_EXPORT = "导出";
    public String LOG_TYPE_DEPLOY = "部署";
    public String LOG_TYPE_START = "启动";

    public String LOG_DETAIL_USER_LOGIN = "用户登陆";
    public String LOG_DETAIL_USER_LOGOUT = "用户退出";
    public String LOG_DETAIL_UPDATE_PROFILE = "更新用户资料";
    public String LOG_DETAIL_UPDATE_PROFILEHEAD = "更新用户头像";
    public String LOG_DETAIL_SYSCONFIG = "系统设置";
    public String LOG_DETAIL_UPDATE_PASSWORD = "更新密码";

    public String REGISTER_EXIT="用户已存在";//用户已存在
    public String REGISTER_SUCCESSFUL="注册成功";//注册成功

}
